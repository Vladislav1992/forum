<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Создать Тему </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=111" type="text/css"> 
 </head>
 <body>
  <div class = "wrapper">
   <header class = "header">
	<ul>
	 <li> <a href = "subindex.php"> HOME </a></li>
	 <li> <a href = "#"> ABOUT US </a></li>
	 <li> <a href = "#"> CONTACTS </a></li>
<?php
    if (!empty($_SESSION['auth'])){echo "<li><a href = \"profile.php\"> MY PROFILE </a></li><li><a href = \"admin?&ask=1\">ADMIN PANEL</a><li><a href = \"theme.php\"> CREATE THEME </a></li><li><a href = \"admin/logout.php\"> LOGOUT </a></li>";} else {echo "<li><a href = \"admin/login.php\"> ENTER IN PROFILE </a></li><li><a href = \"admin/adminLogin.php\"> ENTER AS ADMIN </a></li><li><a href = \"admin/register.php\"> REGISTER NOW </a></li>";}    
?>	
	</ul>
   </header>
   <?php
   include('baza.php');
   
   $query = "SELECT MAX(`id`) AS max FROM forum_topics";
   $count = mysqli_fetch_assoc(mysqli_query($link, $query))['max'];
   //var_dump($count);
   $new_theme = $count + 1;
   //var_dump($new_theme);
   ?>
   <div class = "detail_wrapper">
   <?php
   include('baza.php');
   
   if (!empty($_SESSION['auth']) && !empty($_GET['theme'])) {
	        $topic = $_GET['theme'];
		    $text = $_GET['text'];
			$user = $_SESSION['user_id'];
			
		    $query = "INSERT INTO forum_topics SET title='$topic', status = 0";
		    mysqli_query($link, $query) or die(mysqli_error($link));
            }
			
	if (!empty($_SESSION['auth']) && !empty($_GET['text'])) {
		    $text = $_GET['text'];
			$user = $_SESSION['user_id'];
		    $time = date('Y-m-d H:i:s', time());
			
		    $query = "INSERT INTO forum_texts SET text='$text', status = 0, user_id = '$user', topic_id = '$new_theme', date = '$time'";
		    mysqli_query($link, $query) or die(mysqli_error($link));
	        }		
			?>
    <div class="info alert alert-info">
			<?php
			if(!empty($_SESSION['auth'])){
			if(!empty($_REQUEST) && !empty($_REQUEST['submit'])){
				if(empty($_REQUEST['theme']) && !empty($_REQUEST['text'])){echo 'Введите пожалуйста Ваше название Темы заново!';};
				if(empty($_REQUEST['text']) && !empty($_REQUEST['theme'])){echo 'Введите пожалуйста Ваше сообщение заново!';};
				if(empty($_REQUEST['theme']) && empty($_REQUEST['text'])){echo 'Введите пожалуйста Ваше название Темы и сообщение заново!';};
				if(!empty($_REQUEST['theme']) && !empty($_REQUEST['text'])){echo 'Запись успешно сохранена! После модерации новая Тема и Ваше сообщение появятся на сайте';}
			}
			}
			?>	
	</div>
    <div id="form">
	 <form action="" method="GET">
	  <p><input name = "theme" class="form-control" placeholder="Название Темы"></p>
	  <p><textarea name = "text" class="form-control"
	  <?php
	  if(empty($_SESSION['auth'])){echo 
	  "placeholder=\"Оставлять комментарии могут только зарегестрированные пользователи\"";} else {echo
	  "placeholder=\"Ваше сообщение\"";}
	  ?>
	  ></textarea></p>
	  <p><input type="submit" class="btn btn-info btn-block" value="Сохранить" name="submit" ></p>
	 </form>
	</div>
   </div>
  </body>
</html>  