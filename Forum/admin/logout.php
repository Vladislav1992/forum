<?php
   session_start();
   $_SESSION['auth'] = null;
   $_SESSION['status'] = null;
   $_SESSION['login'] = null;
   $_SESSION['user_id'] = null;
   
   header('Location: /');