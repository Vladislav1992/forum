<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Игровой Форум </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=104" type="text/css"> 
 </head>
 <body>
  <div class = "wrapper">
   <header class = "header">
	<ul>
	 <li> <a href = "subindex.php"> HOME </a></li>
	 <li> <a href = "#"> ABOUT US </a></li>
	 <li> <a href = "#"> CONTACTS </a></li>
<?php
    if (!empty($_SESSION['auth'])){echo "<li><a href = \"profile.php\"> MY PROFILE </a></li><li><a href = \"admin?&ask=1\">ADMIN PANEL</a><li><a href = \"theme.php\"> CREATE THEME </a></li><li><a href = \"admin/logout.php\"> LOGOUT </a></li>";} else {echo "<li><a href = \"admin/login.php\"> ENTER IN PROFILE </a></li><li><a href = \"admin/adminLogin.php\"> ENTER AS ADMIN </a></li><li><a href = \"admin/register.php\"> REGISTER NOW </a></li>";}    
?>	
	</ul>
   </header>
   <section class = "center">
	 <img src = "images/history.png" alt = "history"> 
   </section>
   <div class = "navigation">
    <nav>
	 <ul class="pagination">
<?php   
   if(isset($_GET['page'])){	
   $page = $_GET['page'];} else{
   $page = 1;}
   
   $notesOnPage = 2;
   $from = ($page - 1) * $notesOnPage;
     
   include('baza.php');
   
   $query = "SELECT COUNT(*) as count FROM forum_topics WHERE status > 0";
   $result = mysqli_query($link, $query) or die(mysqli_error($link));
   $count = mysqli_fetch_assoc($result)['count'];
   if($count > 0){  
   $pagesCount = ceil($count / $notesOnPage);
   
   if($page != 1){
   $prev = $page - 1;
   echo 
   "<li>
     <a href=\"?page=$prev\"  aria-label=\"Previous\">
     <span aria-hidden=\"true\">&laquo;</span></a>
	</li>";
   }
   
   for ($i = 1; $i <= $pagesCount; $i++){
   if ($page == $i){
   echo "<li class=\"active\"><a href=\"?page=$i\">$i</a></li>";
   } else {
   echo "<li><a href=\"?page=$i\">$i</a></li>";
   }
   }
   
    
   if($page != $pagesCount){
   $next = $page + 1;
   echo 
   "<li>
     <a href=\"?page=$next\"  aria-label=\"Next\">
     <span aria-hidden=\"true\">&raquo;</span></a>
    </li>";
    } 
   }    
?>	 
	 </ul>
	</nav>
   </div>
   <main>
<?php
   include('baza.php');
        
   $query = "SELECT forum_topics.id, forum_topics.title AS titles, COUNT(forum_texts.topic_id) AS count_topics,
 MAX(forum_texts.date) AS max_dates
 FROM `forum_texts`  
LEFT JOIN forum_topics ON forum_texts.topic_id=forum_topics.id
LEFT JOIN forum_users ON forum_texts.user_id =  forum_users.id
WHERE forum_topics.status > 0 AND forum_texts.status > 0
GROUP BY forum_topics.id LIMIT $from,$notesOnPage";
   $result = mysqli_query($link, $query) or die (mysqli_error($link));
   for ($data = [ ]; $row = mysqli_fetch_assoc($result); $data[ ] = $row);
   //var_dump($topics);
   //var_dump($data);
   echo '<br>';
   echo            "<table border='1'>";
   echo
        "<tr>					 
                      <th class=\"thh\">Forum</th>
					  <th class=\"thh\">ALL MESSAGES</th>
					  <th class=\"thh\">LAST MESSAGE</th>
                      <th class=\"thh\">AUTHOR</th>					  
                     </tr>";
	foreach($data as $elem){
	$chapter = $elem['id'];
	
	echo				 "<tr>
          <td class=\"topics\"><a href =\"detail.php?&url=".$elem['id']."\">{$elem['titles']}</a></td>
          <td>{$elem['count_topics']}</td>";
		  
		  include('baza.php');
		  
		  $query = "SELECT COUNT(*) as count FROM forum_texts WHERE forum_texts.topic_id ='$chapter' AND forum_texts.status > 0";
		  $result = mysqli_query($link, $query) or die(mysqli_error($link));
		  $count = mysqli_fetch_assoc($result)['count'];
		  if($count < 5 && $count > 0){
		  $num = 1;} else {
          $pagesCount = ceil($count / 5);
          $num = $pagesCount;
		  }
   
	echo "<td>{$elem['max_dates']}<br><a href =\"detail.php?&url=".$elem['id']."&page=".$num."\">Go to last message</a></td>
   ";
		  include('baza.php');
		  $top_id = $elem['id'];
		  $query = "SELECT forum_users.login AS login
FROM forum_texts
LEFT JOIN forum_users ON forum_texts.user_id = forum_users.id
	WHERE forum_texts.topic_id = '$top_id' AND forum_texts.status > 0 ORDER BY date DESC LIMIT 1";
    $login = mysqli_fetch_assoc(mysqli_query($link, $query));
	//var_dump($login);
	echo "<td>{$login['login']}</td>
	                    </tr>";
	}
   echo            '</table>';
   //var_dump($_GET);
   //echo '<br>';
   //var_dump($_POST);
   //var_dump($_SESSION);
?>
   </main>
   <footer>
    <p><img src = "images/company.png" alt = "company"></p>
	<p>Copyright © 2001 - 2021  Forum.yes</p>
   </footer>
   </div>
 </body>   
</html>   