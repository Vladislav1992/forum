<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
   <meta charset = "utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title> 
   <?php 
   include('baza.php');
   
   if(isset($_GET['url'])){
   $url = $_GET['url'];
   $_SESSION['url'] = $_GET['url'];
   } else {
   $url = $_SESSION['url'];
   }
   
   $query = "SELECT title FROM forum_topics WHERE id='$url'";
   $result = mysqli_query($link, $query) or die (mysqli_error($link)); 
   $title = mysqli_fetch_assoc($result);
   
   echo $title['title'];
   ?>
    </title>
    <link rel = "stylesheet" href = "style.css?v=110" type="text/css">
  </head>
  <body>
   <div class = "wrapper">
    <header class = "header">
	 <ul>
	  <li> <a href = "subindex.php"> HOME </a></li>
	  <li> <a href = "#"> ABOUT US </a></li>
	  <li> <a href = "#"> CONTACTS </a></li>
<?php
    if (!empty($_SESSION['auth'])){echo "<li><a href = \"profile.php\"> MY PROFILE </a></li><li><a href = \"admin?&ask=1\">ADMIN PANEL</a><li><a href = \"theme.php\"> CREATE THEME </a></li><li><a href = \"admin/logout.php\"> LOGOUT </a></li>";} else {echo "<li><a href = \"admin/login.php\"> ENTER IN PROFILE </a></li><li><a href = \"admin/adminLogin.php\"> ENTER AS ADMIN </a></li><li><a href = \"admin/register.php\"> REGISTER NOW </a></li>";}      
?>	
	 </ul>
    </header>
	<div class = "detail_wrapper">
     <div class = "detail_navigation">
      <nav>
	   <ul class="pagination">
<?php	
   include('baza.php');
   
   if(isset($_GET['page'])){	
   $page = $_GET['page'];} else{
   $page = 1;}

   $notesOnPage = 5;

   $from = ($page - 1) * $notesOnPage;
   
   $query = "SELECT COUNT(*) as count FROM forum_texts WHERE forum_texts.topic_id ='$url' AND forum_texts.status > 0";
   $result = mysqli_query($link, $query) or die(mysqli_error($link));
   //var_dump($result);
   $count = mysqli_fetch_assoc($result)['count'];
   if($count > 1){  
   $pagesCount = ceil($count / $notesOnPage);
   
   if($page != 1){
   $prev = $page - 1;
   echo 
   "<li>
     <a href=\"?page=$prev&url=".$url."\"  aria-label=\"Previous\">
     <span aria-hidden=\"true\">&laquo;</span></a>
	</li>";
   }
   
   for ($i = 1; $i <= $pagesCount; $i++){
   if ($page == $i){
   echo "<li class=\"active\"><a href=\"?page=$i&url=".$url."\">$i</a></li>";
   } else {
   echo "<li><a href=\"?page=$i&url=".$url."\">$i</a></li>";
   }
   }
   
    
   if($page != $pagesCount){
   $next = $page + 1;
   echo 
   "<li>
     <a href=\"?page=$next&url=".$url."\"  aria-label=\"Next\">
     <span aria-hidden=\"true\">&raquo;</span></a>
    </li>";
    } 
   }    
   
   //var_dump($_SERVER['REQUEST_URI']);
?>
	  </ul>
	 </nav>
    </div>
    <main>
<?php
   include('baza.php');
   
   if (!empty($_SESSION['auth']) && !empty($_GET['text'])) {
		    $text = $_GET['text'];
			$user = $_SESSION['user_id'];
			$topic = $_SESSION['url'];
		    $time = date('Y-m-d H:i:s', time());
			
		    $query = "INSERT INTO forum_texts SET text='$text', status = 0, user_id = '$user', topic_id ='$topic', date='$time'";
		    mysqli_query($link, $query) or die(mysqli_error($link));
            }

			$query = "SELECT * FROM forum_texts WHERE forum_texts.topic_id='$url' AND forum_texts.status > 0 ORDER BY date ASC LIMIT $from,$notesOnPage";
            $result = mysqli_query($link, $query) or die (mysqli_error($link));
            for ($data = [ ]; $row = mysqli_fetch_assoc($result); $data[ ] = $row);
			
			$result = '';
			//var_dump($data);
			
			foreach($data as $elem){
			$result.= 
			"<div class=\"note\">
				<p>
				    <span class=\"date\">".$elem['date']."</span><br>";
			include('baza.php');
		  $top_id = $elem['user_id'];
		  $query = "SELECT forum_users.login FROM forum_users WHERE id = '$top_id'";
    $login = mysqli_fetch_assoc(mysqli_query($link, $query));
	$result.=    "<span class=\"name\">".$login['login']."</span>
					</p>
				<p>".$elem['text'].
			   "</p>
			</div><br>";
			}
			
			echo $result;
			//var_dump($_SESSION);
			?>
			<div class="info alert alert-info">
			<?php
			if(!empty($_SESSION['auth'])){
			if(!empty($_REQUEST) && !empty($_REQUEST['submit'])){
				if(empty($_REQUEST['text'])){echo 'Введите пожалуйста Ваше сообщение заново!';};
				if(!empty($_REQUEST['text'])){echo 'Запись успешно сохранена!';}
			}
			}
			?>	
			</div>
			<div id="form">
				<form action="" method="GET">
					<p><textarea name = "text" class="form-control"
					<?php
					if(empty($_SESSION['auth'])){echo 
				    "placeholder=\"Оставлять комментарии могут только зарегестрированные пользователи\"";} else {echo
					"placeholder=\"Ваше сообщение\"";}
					?>
					></textarea></p>
					<p><input type="submit" class="btn btn-info btn-block" value="Сохранить" name="submit" ></p>
				</form>
			</div>
     </main>
	</div> 
	<footer>
    <p><img src = "images/company.png" alt = "company"></p>
	<p>Copyright © 2001 - 2021  Forum.yes</p>
    </footer>
   </div>
  </body>
</html>